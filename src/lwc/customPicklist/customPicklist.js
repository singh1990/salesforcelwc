import { LightningElement,api,track, wire } from 'lwc';
import getpicklistmethod from '@salesforce/apex/GenericUtility.getselectOptions';

export default class customPicklist extends LightningElement {
    @api    objectname ="Case";
    @api    fieldname ="Type";
    @api    valuefield;
    @api    label;
    @api    required;
    @api    defaultval;
    @track  fieldpicklist;
    @track  showcombolist= false;
    @track  fieldcomobox;
    @track  displaycomboresult;
    @track  picklistvalues =[];
    @track  error;
    @track  selectcomboval;
    @track  txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    

    @wire(getpicklistmethod,{'sObjectName':'$objectname','fieldapi':'$fieldname','defaultvalue':'$defaultval'})
    getdata({ error, data }){
        if(data){
            debugger;
            console.log(data);
            const retunval = JSON.parse(data);
            this.picklistvalues = retunval.picklistvallist;
            if(retunval.fieldtype ==='COMBOBOX'){
               this.fieldpicklist = false;
               this.fieldcomobox  = true; 
            }
            else{
                this.fieldpicklist = true;
               this.fieldcomobox  = false;
            }
            
        }
        else if(error){
            this.error = null;
        }
    }

    handleKeyChange(event) {
        
        var selectedval = event.target.value;
        this.valuefield = selectedval;
        //alert('event.target.value '+this.valuefield);
        const picklistSelectEvent =  new CustomEvent('picklistselect',{detail: selectedval});
        this.dispatchEvent(picklistSelectEvent);
    }

    handelcomboclick(){
        this.showcombolist = true;
    }

    handelcombochange(event){
       debugger;
        const currentText = event.currentTarget.dataset.id;
        this.selectRecordName = event.currentTarget.dataset.name;
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        this.selectcomboval = currentText;
        //alert('event.target.value '+this.currentText);
        const picklistSelectEvent =  new CustomEvent('picklistselect',{detail: currentText});
        this.dispatchEvent(picklistSelectEvent);
        this.showcombolist = false;
    }

    handelbomboboxvaluechange(event){
        debugger;
        this.showcombolist = false;
        this.selectRecordName = event.target.value;
        //alert('event.target.value '+this.selectRecordName);
        const picklistSelectEvent =  new CustomEvent('picklistselect',{detail: this.selectRecordName});
        this.dispatchEvent(picklistSelectEvent);
        this.showcombolist = false;
    }
}