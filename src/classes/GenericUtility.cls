public  class GenericUtility {
   
   @AuraEnabled(cacheable=true)
  public static string getselectOptions(String sObjectName,string fieldapi,string defaultvalue) {
       Map<String,List<String>> fieldToPicklistValuesMap = new Map<String,List<String>>();
       list<picklistwrap> picklistval = new list<picklistwrap>();
       retrunwrap returnstr = new retrunwrap();
      if(String.isNotBlank(sObjectName) && String.isNotBlank(fieldapi)){
            // Get a map of fields for the SObject
          map < String, Schema.SObjectField > fieldMap = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();
          Schema.DescribeFieldResult fieldResult  = fieldMap.get(fieldapi).getDescribe();
          returnstr.fieldtype = String.valueof(fieldResult.getType());        
          if(fieldResult.getType()==Schema.DisplayType.PICKLIST || fieldResult.getType()==Schema.DisplayType.COMBOBOX){
                      system.debug('Picklist field '+ fieldapi);
                      fieldToPicklistValuesMap.put(fieldapi,new list<String>());
                      for (Schema.PicklistEntry a:  fieldResult.getPickListValues()) {
                           //fieldToPicklistValuesMap.get(fieldapi).add(a.getValue());
                           boolean isdefault = string.isNotBlank(defaultvalue)?(defaultvalue==(String)a.getValue()):false;
                           picklistval.add(new picklistwrap((String)a.getValue(),(string)a.getLabel(),isdefault));
                          }
                  } 
      }
      
      returnstr.picklistvallist = picklistval;
      
      return JSON.serialize(returnstr);
  }

  public class picklistwrap{
      string apival;
      string labelval;
      boolean isdefault;
      public picklistwrap(string apivalue,string labelvalue,boolean defaultval){
             this.apival = apivalue;
             this.labelval = labelvalue;
             this.isdefault = defaultval;
      }

  }
    
    public class retrunwrap{
        public string fieldtype;
        public list<picklistwrap> picklistvallist;
    }
}